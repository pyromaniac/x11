# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: Qt Shader Tools"
DESCRIPTION="The Qt Shader Tools module builds on the SPIR-V Open Source
Ecosystem. For compiling into SPIR-V glslang is used, while translating and
reflecting is done via SPIRV-Cross.
In order to allow shader code to be written once in Qt applications and
libraries, all shaders are expected to be written in a single language which
is then compiled into SPIR-V. This shanding language is Vulkan-compatible
GLSL at the moment."

LICENCES="GPL-3"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
"

qtshadertools_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtshadertools_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

