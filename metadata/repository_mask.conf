(
    app-benchmarks/glmark2[~scm]
    dev-libs/libepoxy[~scm]
    media-libs/glew[~scm]
    x11-apps/xinit[~scm]
    x11-apps/xkeyboard-config[~scm]
    x11-dri/glu[~scm]
    x11-dri/libdrm[~scm]
    x11-dri/mesa[~scm]
    x11-dri/mesa-demos[~scm]
    x11-drivers/intel-driver[~scm]
    x11-drivers/xf86-input-evdev[~scm]
    x11-drivers/xf86-input-libinput[~scm]
    x11-drivers/xf86-input-mouse[~scm]
    x11-drivers/xf86-input-synaptics[~scm]
    x11-drivers/xf86-video-amdgpu[~scm]
    x11-drivers/xf86-video-ati[~scm]
    x11-drivers/xf86-video-dummy[~scm]
    x11-drivers/xf86-video-intel[~scm]
    x11-drivers/xf86-video-nouveau[~scm]
    x11-drivers/xf86-video-virgl[~scm]
    x11-misc/rofi[~scm]
    x11-libs/libpciaccess[~scm]
    x11-libs/libX11[~scm]
    x11-libs/libXext[~scm]
    x11-libs/libXi[~scm]
    x11-libs/libxkbcommon[~scm]
    x11-libs/pixman[~scm]
    x11-libs/virglrenderer[~scm]
    x11-themes/oxygen-gtk2[~scm]
    x11-themes/oxygen-gtk3[~scm]
    x11-utils/util-macros[~scm]
    x11-wm/fluxbox[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

media-libs/freetype:2[<2.10.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 20 Oct 2020 ]
    token = security
    description = [ CVE-2020-15999 ]
]]

x11-apps/xkeyboard-config[<=2.4.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jan 2012 ]
    token = security
    description = [ CVE-2012-0064 ]
]]

app-text/poppler[<22.09.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 02 Sep 2022 ]
    token = security
    description = [ CVE-2022-38784 ]
]]

x11-server/xwayland[<23.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Jan 2024 ]
    token = security
    description = [ CVE-2023-6816, CVE-2024-{0229,0408,0409,21885,21886} ]
]]

x11-libs/libXv[<1.0.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1989 and CVE-2013-2066 ]
]]

x11-libs/libXrandr[<1.4.1] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1986 ]
]]

x11-libs/libXext[<1.3.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1982 ]
]]

x11-libs/libXtst[<1.2.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2063 ]
]]

x11-libs/libXxf86vm[<1.1.3] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2001 ]
]]

x11-libs/libXt[<1.1.4] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2002 and CVE-2013-2005 ]
]]

x11-libs/libXres[<1.0.7] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1988 ]
]]

x11-libs/libXinerama[<1.1.3] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1985 ]
]]

x11-libs/libXcursor[<1.1.15] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16612 ]
]]

x11-libs/libXfixes[<5.0.1] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1983 ]
]]

x11-libs/libXp[<1.0.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-2062 ]
]]

x11-libs/libXxf86dga[<1.1.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1991 and CVE-2013-2000 ]
]]

x11-libs/libdmx[<1.1.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1992 ]
]]

x11-libs/libFS[<1.0.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1996 ]
]]

x11-libs/libX11[<1.8.7] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Oct 2023 ]
    token = security
    description = [ CVE-2023-43785, CVE-2023-43786, CVE-2023-43787 ]
]]

x11-libs/libxcb[<1.9.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Jun 2013 ]
    token = security
    description = [ CVE-2013-2064 ]
]]

x11-libs/libXi[<1.7.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1998, CVE-2013-1984, CVE-2013-1995 ]
]]

x11-libs/libXrender[<0.9.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1987 ]
]]

x11-libs/libXvMC[<1.0.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1999 ]
]]

x11-libs/libXfont[<1.5.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16611 ]
]]

net-libs/libvncserver[<0.9.14] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Feb 2023 ]
    token = security
    description = [ CVE-2020-29260 ]
]]

x11-libs/libvdpau[<1.1.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Sep 2015 ]
    token = security
    description = [ CVE-2015-5{198,199,200} ]
]]

x11-libs/qtwebengine:5[<5.15.12_p20231208] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 29 Dec 2023 ]
    token = security
    description = [ CVE-2023-{5218,5482,5849,5996,5997,6112,45853} ]
]]

media-libs/fontconfig[<2.12.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Aug 2016 ]
    token = security
    description = [ CVE-2016-5384 ]
]]

x11-libs/libXpm[<3.5.17] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Oct 2023 ]
    token = security
    description = [ CVE-2023-43788, CVE-2023-43789 ]
]]

x11-libs/libXfont2[<2.0.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16611 ]
]]

(
    x11-libs/qtbase[<5.11.3]
    x11-libs/qtimageformats[<5.11.3]
    x11-libs/qtvirtualkeyboard[<5.11.3]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 04 Dec 2018 ]
    *token = security
    *description = [ https://blog.qt.io/blog/2018/12/04/qt-5-11-3-released-important-security-updates/ ]
]]

x11-apps/xdm[<1.1.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Mar 2019 ]
    token = security
    description = [ CVE-2013-2179 ]
]]

x11-libs/libXdmcp[<1.1.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Mar 2019 ]
    token = security
    description = [ CVE-2017-2625 ]
]]

(
    x11-libs/qtbase:5[<5.15.12_p151]
    x11-libs/qtbase:6[<6.6.2]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 02 Jan 2024 ]
    *token = security
    *description = [ CVE-2024-25580 ]
]]

media-gfx/fontforge[<20200314] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Sep 2020 ]
    token = security
    description = [ CVE-2020-5395, CVE-2020-5496 ]
]]

x11-libs/gdk-pixbuf[<2.42.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Jan 2021 ]
    token = security
    description = [ CVE-2020-29385 ]
]]

(
    x11-libs/qtsvg:5[<5.15.9_p8]
    x11-libs/qtsvg:5[>=6.0.0&<6.5.0-r1]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 15 May 2023 ]
    *token = security
    *description = [ CVE-2023-32573 ]
]]

x11-libs/qtsvg:6[<6.6.0-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 May 2023 ]
    token = security
    description = [ CVE-2023-45872 ]
]]

x11-server/xorg-server[<21.1.11] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Jan 2024 ]
    token = security
    description = [ CVE-2023-6816, CVE-2024-{0229,0408,0409,21885,21886} ]
]]

x11-libs/pixman[<0.42.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Nov 2022 ]
    token = security
    description = [ CVE-2022-44638 ]
]]

x11-libs/cairo[<1.17.4] [[
    author = [ Calvin Walton <calvin.walton@kepstin.ca> ]
    date = [ 10 Nov 2022 ]
    token = security
    description = [ CVE-2020-35492 ]
]]

x11-apps/xterm[<385] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Oct 2023 ]
    token = security
    description = [ CVE-2023-40359 ]
]]

x11-apps/rxvt-unicode[<9.31] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Feb 2023 ]
    token = security
    description = [ CVE-2022-4170 ]
]]
